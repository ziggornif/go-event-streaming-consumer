# Go events streaming example with JetStream

Consumer part

## Install

```sh
go install
```

## Run

```sh
go run main.go
```
