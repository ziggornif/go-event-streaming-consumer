package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/nats-io/nats.go"
)

type Event struct {
	MessageType string
	ID          string
	Message     string
	Date        time.Time
	Author      string
}

func main() {
	var events []string

	// Connect to NATS
	nc, _ := nats.Connect(nats.DefaultURL)
	js, err := nc.JetStream()
	if err != nil {
		log.Fatal(err)
	}
	// Create durable consumer monitor
	js.Subscribe("TWITTERCLONE.tweet_created", func(msg *nats.Msg) {
		msg.Ack()
		var event Event
		err := json.Unmarshal(msg.Data, &event)
		if err != nil {
			log.Fatal(err)
		}
		events = append(events, fmt.Sprintf("New tweet from %v : %v", event.Author, event.Message))

	}, nats.Durable("monitor"), nats.ManualAck())

	http.HandleFunc("/events", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		resp, _ := json.Marshal(events)
		w.Write(resp)
		events = []string{}
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "listener.html")
	})

	err = http.ListenAndServe(":8081", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
